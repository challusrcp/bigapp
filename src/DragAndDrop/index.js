import React, { Component } from 'react';

import './index.css';
import tasks from '../data/index';

const CONSTANTS = {
    PENDING: 'pending',
    COMPLETE : 'complete',
    LEFT: 'left',
    RIGHT: 'right',
}

class DragAndDrop extends Component {
    constructor() {
        super();
        this.state = {
            tasks: [],
            selectedItems : [],
            pendingData :[],
            completeData:[]
        }
    }
    
    componentDidMount(){
        this.setState({tasks: tasks});
    }
    onDragOver = (ev) => {
        ev.preventDefault();
    };

    onDragStart = (ev, id) => {
        this.setState({pendingData:[],completeData:[]});
        ev.dataTransfer.setData("id", id);
    };
 
    onDrop = (ev, cat) => {       // when dropped in droppable area
        let id = ev.dataTransfer.getData("id");
        let tasks = this.state.tasks.filter((task) => { // changes the category of single task and sets the state
            if (task.id === id) {
                     task.category = cat;           
            }              
             return task;       
         });        
         this.setState({           
            ...this.state,           
            tasks       
         });    
      }

      handleClick = (id, category) => { // handles selection of tasks
          if(category === CONSTANTS.PENDING){
            let arr = this.state.pendingData;
            arr.push(id);
            let unique = [...new Set(arr)]; 
            this.setState({pendingData: unique, completeData:[]});
          } else {
            let arr = this.state.completeData;
            arr.push(id);
            let unique = [...new Set(arr)]; 
            this.setState({completeData: unique, pendingData:[]});
          }
      }
      
      isTaskSelected = (task, selectedArray) => { // checks if task is present in selected list
        return selectedArray.some((swapTask)=>{
            return task.id === swapTask;
        })
      }
      
      constructTasksArray = (selectedArray, assignCategory) => { //sets new array according to selecetd tasks
        let allTasks = Object.assign([],this.state.tasks);
         allTasks.forEach((task) => {
            if(this.isTaskSelected(task,selectedArray)){
                task.category = assignCategory;
                console.log(assignCategory);
            }
        });
        return allTasks;
      };

      handleSwapButtonClick = (direction) => { // invoked on click of swap buttons
            if(direction === CONSTANTS.LEFT && this.state.completeData.length){
                let completed = this.state.completeData;
                let newTasks = this.constructTasksArray(completed, CONSTANTS.PENDING);
                this.setState({tasks: newTasks,completeData:[],pendingData:[]});
            }else if(direction === CONSTANTS.RIGHT && this.state.pendingData.length){
                let pending = this.state.pendingData;
                let newTasks = this.constructTasksArray(pending, CONSTANTS.COMPLETE);
                this.setState({tasks: newTasks,completeData:[],pendingData:[]});
            }else{
                alert('select tasks to shift');
            }
      };

      isSelected = (task) => { //checks if a task is selected(to assign style)
        if(task.category === CONSTANTS.PENDING){
            return this.state.pendingData.some((uniqueId)=> uniqueId===task.id);
        } else {
            return this.state.completeData.some((uniqueId)=> uniqueId===task.id);
        }
      };

      classifyTasksOnCategory = (allTasks) => { // seperates tasks into pending and compplete
        let tasks = {
            pending: [],
            complete: []
        };
        allTasks.length && allTasks.forEach((task) => {
            let style= this.isSelected(task)?{backgroundColor:'antiquewhite'}:{backgroundColor: '#fff'};
            tasks[task.category].push(<div
                key={task.id}
                onDragStart={(e) => this.onDragStart(e, task.id)}
                draggable
                onClick={()=>{this.handleClick(task.id, task.category)}}
                className="draggable"
                style={{textAlign:'center',padding:'10px',marginTop:'5px',marginBottom:'5px',...style}}>
                {task.name}
            </div>);
        });
        return tasks;
      };

      handleClearSelection = () => {
          this.setState({completeData:[],pendingData:[]});
      }

    renderDropAreaHeaders = (text) => {
        return (<div className="task-header">
            <span>{text}</span>
        </div>)
    };

    renderHandlerButtons = () => {
        return (
            <div style={{
                display: 'flex', justifyContent: 'space-around',
                alignItems: 'center', cursor: 'pointer', flexDirection:'column'
            }} className="col-md-2">
                <div> <span onClick={() => this.handleSwapButtonClick('left')} className='buttonStyle'><i class="fas fa-caret-right"></i></span></div>
                <div> <span onClick={() => this.handleSwapButtonClick('right')} className='buttonStyle' ><i class="fas fa-caret-left"></i></span></div>
                <div> <span onClick={() => this.handleClearSelection()} style={{ padding: '10px', backgroundColor: 'rgba(255,255,255,0.5)' }}>Clear Selection</span></div>
            </div>
        );
    }

    render() {
        let tasks = this.classifyTasksOnCategory(this.state.tasks);
        return (
            <div className="col-md-12 wrapper">
                <div>
                    <h1 style={{textAlign:'center',color:'#fff',marginBottom:'10px'}}>{"Drag and drop & multi select click and move"}</h1>
                </div>
               {this.state.tasks.length>0?
                <div className="row mt40">
                <div className="col-md-1"></div>
                    <div className="col-md-4">
                        <div style={{padding:'10px',backgroundColor:'lightgrey',height: '500px',overflow:'auto'}} 
                            className="pending"
                            onDragOver={(e) => this.onDragOver(e)}
                            onDrop={(e) => { this.onDrop(e, CONSTANTS.PENDING) }}>
                               {this.renderDropAreaHeaders('PENDING TASKS')}
                            {tasks.pending}
                        </div>
                    </div>
                      {this.renderHandlerButtons()}
                    <div className="col-md-4"> 
                        <div style={{padding:'10px',backgroundColor:'lightgrey',height: '500px',overflow:'auto'}} className="droppable"
                            onDragOver={(e) => this.onDragOver(e)}
                            onDrop={(e) => this.onDrop(e, CONSTANTS.COMPLETE)}>
                            {this.renderDropAreaHeaders('COMPLETED TASKS')}
                            {tasks.complete}
                        </div>
                    </div>
                    <div className="col-md-1"></div>
                </div>:null}
            </div>);
    }
}

export default DragAndDrop;