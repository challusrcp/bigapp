import React from 'react';
import { Route, Router } from 'react-router-dom';
import App from './App';
import DragComponent from './DragAndDrop/index';
import Callback from './Callback/Callback';
import {OverView} from './Table/table'
import Auth from './Auth/Auth';
import history from './history';

const auth = new Auth();

const handleAuthentication = ({location}) => {
  if (/access_token|id_token|error/.test(location.hash)) {
    auth.handleAuthentication();
  }
};

export const makeMainRoutes = () => {
  return (
      <Router history={history}>
        <div>
          <Route path="/" render={(props) => <App auth={auth} {...props} />} />
          <Route path="/drag-list" render={(props) => <DragComponent auth={auth} {...props} />} />
            <Route path="/table" render={(props) => <OverView auth={auth} {...props} />} />
            <Route path="/callback" render={(props) => {
            handleAuthentication(props);
            return <Callback {...props} />
          }}/>
        </div>
      </Router>
  );
};
