const tasks = [{
    id: "1",
    name: "Task1",
    category: "pending",
},
{
    id:"2",
    name: "Task2",
    category: "pending",
},
{
    id:"3",
    name: "Task3",
    category: "pending",
},
{
    id:"4",
    name: "Task4",
    category: "pending",
},{
    id:"5",
    name: "Task5",
    category: "pending",
},{
    id:"6",
    name: "Task6",
    category: "pending",
},{
    id:"7",
    name: "Task7",
    category: "complete",
},{
    id:"8",
    name: "Task8",
    category: "complete",
},
{
    id:"9",
    name: "Task9",
    category: "complete",
},
{
    id:"10",
    name: "Task10",
    category: "complete",
},
]

export default tasks;