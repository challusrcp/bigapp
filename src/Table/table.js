import './index.css';
import * as React from 'react';
import {
    ColumnDirective,
    ColumnsDirective, Filter,
    GridComponent,
    Inject, Page, Sort, Toolbar, VirtualScroll,
} from '@syncfusion/ej2-react-grids';

export function nameTemplate(props) {
    return (
        <div>
            <div className={'name-div'}>{props.name}</div>
            <div className={'email-div'}>{props.email}</div>
        </div>
    );
}

export class OverView extends React.Component {
    constructor() {
        super(...arguments);
        this.check = {
            type: 'CheckBox'
        };
        this.select = {
            persistSelection: true,
            type: "Multiple",
            checkboxOnly: true,
        };
        this.toolbarOptions = ['Search'];
        this.Filter = {
            type: 'Menu'
        };
        this.pageSettings = { pageCount: 5, pageSizes: true, pageSize: 10 };
        this.state = {
            data: []
        }
    }

    componentDidMount() {
        const self = this;
        fetch('https://jsonplaceholder.typicode.com/users')
            .then(function (response) {
                return response.json();
            })
            .then(function (myJson) {
                self.setState({
                    data: myJson
                });
                console.log(myJson);
            });
    }

    render() {
        return (<div className='control-pane'>
            <div className='control-section'>
                <GridComponent id="overviewgrid"
                               dataSource={this.state.data}
                               toolbar={this.toolbarOptions}
                               pageSettings={this.pageSettings}
                               enableHover={false}
                               enableVirtualization={true}
                               rowHeight={38}
                               height='400'
                               filterSettings={this.Filter}
                               selectionSettings={this.select}
                               allowFiltering
                               allowSorting
                               allowPaging
                               allowSelection
                >
                    <ColumnsDirective>
                        <ColumnDirective type='checkbox'
                                         allowSorting={false}
                                         allowFiltering={false}
                                         width='60'/>
                        <ColumnDirective field='id'
                                         visible={false}
                                         headerText='Employee ID'
                                         isPrimaryKey={true} width='130'/>
                        <ColumnDirective field='name'
                                         headerText='FULL NAME'
                                         width='230'
                                         clipMode='EllipsisWithTooltip'
                                         template={nameTemplate}
                                         filter={this.check}/>
                        <ColumnDirective field='phone'
                                         headerText='PHONE'
                                         width='230'
                                         clipMode='EllipsisWithTooltip'
                                         filter={this.check}/>
                        <ColumnDirective field='username'
                                         headerText='USER NAME'
                                         width='230'
                                         clipMode='EllipsisWithTooltip'
                                         filter={this.check}/>
                        <ColumnDirective field='company.name'
                                         headerText='COMPANY NAME'
                                         filter={this.check}
                                         width='230'/>

                    </ColumnsDirective>
                    <Inject services={[Filter, VirtualScroll, Sort, Toolbar, Page, ]} />

                </GridComponent>
            </div>
            <style>
                @import 'src/grid/Grid/style.css';
            </style>

        </div>);
    }
}

